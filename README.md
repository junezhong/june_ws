**Build Voting app**
The voting app can be built from either maven or from Eclipse.

**Build an executable jar with maven.**

1. ./mvnw clean
2. ./mvnw package
	
3. Start the voting app, run
   java -jar target/votingapp-0.0.1-SNAPSHOT.jar
	
## Build from Elipse

1. Download Eclipse Java EE IDE for Web Developers.
2. Import the project, from Eclipse, choose "File" -> "Open Projects from File System"
3. Start Voting App from Eclipse. Run "VotingappApplication.java" as standalone java application.

## Access the voting app ##

1. go to your brower, type "http://localhost:8080".
2. there are 100 pre-seed users that can be used for test, 
   user name "user1@gmail.com", password "password1",   "user2@gmail.com", password: "password2" and so on.

## Test plan ##
Test plan is at https://docs.google.com/document/d/1rx9mwzxPxDpMM_cNiihjse_0-Fvw-j7ZdeuH3fZaUtE/edit?usp=sharing

