package com.netflix.security.votingapp.controller;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.setup.SharedHttpSessionConfigurer;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import com.netflix.security.votingapp.controller.VotingController;
import com.netflix.security.votingapp.entity.User;
import com.netflix.security.votingapp.repo.UserRepo;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = VotingController.class)
public class VotingControllerTest {

    @Autowired
    private MockMvc mockMvc;
    
    private static List<User> users;
    
    @BeforeClass
    public static void setUpUsers() {
    	UserRepo userRepo = new UserRepo();
    	users = userRepo.findAllUsers();
    }
    
    /**
     * This test tests that an authenticated user is routed to voting page.
     * @throws Exception
     */
    @WithMockUser(value = "user1@gmail.com")
	@Test
	public void testAuthenticatedUserVote() throws Exception {
		mockMvc.perform(get("/"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(view().name("doVote.html"))
            .andExpect(model().attribute("user", hasProperty("userName", equalTo("user1@gmail.com"))));
	}
        
    /**
     * This test tests that an unauthenticated user is directed to error page 
     * @throws Exception
     */
    @WithMockUser(value = "foo@gmail.com")
	@Test
	public void testUnAuthenticatedUserVote() throws Exception {
		mockMvc.perform(get("/"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(view().name("error.html"));
	}
    
    /**
     * This test tests that if user is not logged when user is routed to voting page when trying to access /voteFor.
     * @throws Exception
     */
    @WithMockUser(value = "user2@gmail.com")
	@Test
	public void testVoteForUserNotLoggedIn() throws Exception {
		mockMvc.perform(get("/voteFor").param("movieId", "1"))
			.andExpect(status().is2xxSuccessful())
			.andExpect(view().name("error.html"));
	}
    
    /**
     * This test tests that if user is logged user can vote.
     * @throws Exception
     */
    @WithMockUser(value = "user2@gmail.com")
	@Test
	public void testVoteForUserLoggedIn() throws Exception {
		mockMvc.perform(get("/voteFor").param("movieId",  "1").sessionAttr("user", users.get(1)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(view().name("thankyouforvote.html"));
	}
    
    /**
     * This test tests that if user is logged user can't revote
     * @throws Exception
     */
    @WithMockUser(value = "user3@gmail.com")
	@Test
	public void testVoteForUserLoggedInDuplicateVote() throws Exception {
    	//set up MockMvc to reuse http session
    	MockMvc mockMvcSharedSession = getSharedSessionMvc();
    	mockMvcSharedSession.perform(get("/"));
    	//first time user can vote and is routed to "thankforvote.html" page
    	mockMvcSharedSession.perform(get("/voteFor").param("movieId",  "1").sessionAttr("user", users.get(2)))
				.andExpect(status().is2xxSuccessful())
				.andExpect(view().name("thankyouforvote.html"));
    	//second time when the same user votes the user is routed to "alreadyVoted" page
    	mockMvcSharedSession.perform(get("/voteFor").param("movieId",  "1").sessionAttr("user", users.get(2)))
				.andExpect(view().name("alreadyVoted.html"));
	}
    
    /**
     * This test tests most voted movie
     */
    @WithMockUser(value = "user2@gmail.com")
    @Test
    public void testMostVotedMovie() throws Exception {
    	//user2 votes for movie 1
		mockMvc.perform(get("/voteFor").param("movieId", "1").sessionAttr("user", users.get(2)))
			.andExpect(status().is2xxSuccessful());
		//user4 votes for movie 4
		mockMvc.perform(get("/voteFor").param("movieId", "4").sessionAttr("user", users.get(3)))
			.andExpect(status().is2xxSuccessful());
    	MockMvc mockMvcSharedSession = getSharedSessionMvc();
    	//user5 votes for movie 4
    	mockMvcSharedSession.perform(get("/voteFor").param("movieId", "4").sessionAttr("user", users.get(4)))
			.andExpect(status().is2xxSuccessful());
		//most voted movie is movie with Id = 4 (The Crown) 
    	mockMvcSharedSession.perform(get("/voteresult"))
        	.andExpect(model().attribute("moviename", equalTo("The Crown")));
    }
    
    private MockMvc getSharedSessionMvc() {
    	//set up MockMvc to reuse http session
    	MockMvc mockMvcSharedSession = MockMvcBuilders.standaloneSetup(new VotingController())
    			.apply(SharedHttpSessionConfigurer.sharedHttpSession())
    			.build();
    	return mockMvcSharedSession;
    }
}
