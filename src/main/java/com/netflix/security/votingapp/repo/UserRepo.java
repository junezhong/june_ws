package com.netflix.security.votingapp.repo;

import java.util.ArrayList;
import java.util.List;

import com.netflix.security.votingapp.entity.User;;

/**
 * UserRepo is a mock of a Spring Repository. The "real" UserRepo abstracts User table in DB, and will provide functionalities to 
 * save, find and delete users by using Spring's build-in JPA support. This implementation mocks the "User" table and db operations on the table.
 *
 */
public class UserRepo {

	private static final List<User> allUsers = new ArrayList<>();

	//pre seed users for the coding exercise. 
	static {
		for (int i=1; i<100l; i++) {
			allUsers.add(new User((long)i, String.format("user%d@gmail.com", i)));
		}
	}
	
	/**
	 * find a user by user name. Assume user name is unique.
	 * @param userName 
	 * @return user with the given user name.
	 */
	public User findByUserName(String userName) {
		if ( userName == null || userName.length() == 0 ) {
			return null;
		}
		for (User user : allUsers) {
			if (userName.equals(user.getUserName())) {
				return user;
			}
		}
		return null;
	}
	
	public List<User> findAllUsers() {
		return allUsers;
	}
}
