package com.netflix.security.votingapp.entity;

/**
 * The class represents a user. 
 */
public class User {
    //auto incremented uuid in "User" table. 
	private Long userId;
	private String userName;
	
	public User(Long userId, String userName) {
		this.userId = userId;
		this.userName = userName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
