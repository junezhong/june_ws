package com.netflix.security.votingapp.entity;

/**
 * the class represents a Movie.
 *
 */
public class Movie {

	//auto incremented uuid in "Movie" table. 
	private Long movieId;
	//title of the movie
	private String movieTitle;
	//description of the movie
	private String description;
	//link to an image of the movie
	private String picture;
	
	public Movie(Long movieId, String title) {
		this.movieId = movieId;
		this.movieTitle = title;
	}

	public Movie(Long movieId, String title, String picture) {
		this.movieId = movieId;
		this.movieTitle = title;
		this.picture = picture;
	}
	
	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}

	public String getMovieTitle() {
		return movieTitle;
	}

	public void setMovieTitle(String title) {
		this.movieTitle = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
}
