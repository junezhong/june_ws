package com.netflix.security.votingapp.entity;

public class MovieVote {

	private Long movieId;
	private Long voteCount;
	
	public MovieVote(Long movieId, Long voteCount) {
		super();
		this.movieId = movieId;
		this.voteCount = voteCount;
	}
	
	public Long getMovieId() {
		return movieId;
	}
	
	public void setMovieId(long movieId) {
		this.movieId = movieId;
	}
	
	public Long getVoteCount() {
		return voteCount;
	}
	public void setVoteCount(Long voteCount) {
		this.voteCount = voteCount;
	}
}
