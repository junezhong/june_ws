package com.netflix.security.votingapp.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.netflix.security.votingapp.entity.Movie;
import com.netflix.security.votingapp.entity.User;
import com.netflix.security.votingapp.repo.MovieRepo;
import com.netflix.security.votingapp.repo.UserRepo;

@Controller
public class VotingController {

	public final Logger logger = Logger.getLogger(VotingController.class);
	
	//In "real" code, wire in UserRepo and MovieRepo to leverage Spring's JPA
	//@Autowired
	//UserRepo userRepo;
	
	//@Autowired
	//MovieRepo movieReo
	
	private UserRepo userRepo = new UserRepo();
	private MovieRepo movieRepo = new MovieRepo();
	
	//a hash map to remember numbers of votes for each movie. key is movie id, value is number of the votes for the movie.
	private Map<Long, Long> voteCountMap = new HashMap<>();
	//a hash map to remember which users have voted, for which movies. Key is user id and value is movie id. 
	private Map<Long, Long> voteMap = new HashMap<>();
	
	@RequestMapping("/")
	public String vote(Model model, HttpSession session) {
		User user = null; 
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (!(authentication instanceof AnonymousAuthenticationToken)) {
		    String currentUserName = authentication.getName();
		    logger.info("current logged in user is " + currentUserName);
		    user = userRepo.findByUserName(currentUserName);
		}
		//if user is not authenticated, direct user to login page again
		if (user == null) {
			user = (User)(session.getAttribute("user"));
			if ( user != null ) {
				model.addAttribute("user", user);
			} else {
				return "error.html";
			}
		}
		logger.info(user + " logged");
		session.setAttribute("user", user);
		model.addAttribute("user", user);
		if (! voteMap.containsKey(user.getUserId())) {
			List<Movie> allMovies = movieRepo.findAllMovies();
			model.addAttribute("movies", allMovies);
			return "doVote.html";
		} else {
			model.addAttribute("votedMovie", session.getAttribute("votedMovie"));
			return "alreadyVoted.html";
		}
	}
	
	@RequestMapping("/voteFor")
	public String voteFor(@RequestParam Long movieId, Model model, HttpSession session) {
		User user = (User)(session.getAttribute("user"));
		//if user is not authenticated, direct user to login page again
		if (user == null) {
			return "error.html";
		}
		model.addAttribute("user", user);

		if (!voteMap.containsKey(user.getUserId())) {
			voteMap.put(user.getUserId(), movieId);
		   //find movie
		   Movie movie = movieRepo.findByMovieId(movieId);
		   if (movie != null) {
			   logger.info(user.getUserName() + " voted for " + movieId);
			   voteCountMap.put(movieId, voteCountMap.getOrDefault(movieId, 0l) + 1l);
		   } else {
			   logger.info("the movie " + movieId + " " + user.getUserName() + " doesn't exist");
		   }
		   model.addAttribute("votedMovie", movie);
		   session.setAttribute("votedMovie", movie);
		   return "thankyouforvote.html";
		} else {
		   logger.info(user + " tried to revote");
		   Long votedMovieId = voteMap.get(user.getUserId());
		   logger.info("user already voted for " + movieRepo.findByMovieId(votedMovieId).getMovieTitle());
		   model.addAttribute("votedMovie", movieRepo.findByMovieId(votedMovieId));
		   return "alreadyVoted.html";
		}
	}
	
	@RequestMapping("/voteresult")
	public String voteResult(Model model, HttpSession session) {
		User user = (User)(session.getAttribute("user"));
		model.addAttribute("user", user);

		Map.Entry<Long, Long> favoriteMovie = getFavoriteMovie(voteCountMap);
		if ( favoriteMovie != null ) {
		  Movie movie = movieRepo.findByMovieId(favoriteMovie.getKey());
		  logger.info("favorite movie voted is " + movie.getMovieTitle());
		  if ( movie != null ) {
		    model.addAttribute("moviename", movie.getMovieTitle());
		    model.addAttribute("mostvotedcount", favoriteMovie.getValue());
		  }
		}
		return "voteresult.html";
	}
	
	//@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    if (auth != null){    
	        new SecurityContextLogoutHandler().logout(request, response, auth);
	    }
	    return "doVote.html";//You can redirect wherever you want, but generally it's a good practice to show login screen again.
	}
	
	private static Map.Entry<Long, Long> getFavoriteMovie(Map<Long, Long> map){
		if ( map == null || map.size() == 0 ) return null;
        List<Map.Entry<Long, Long>> entries = new LinkedList<Map.Entry<Long, Long>>(map.entrySet());      
        Collections.sort(entries, new Comparator<Map.Entry<Long, Long>>() {
            @Override
            public int compare(Entry<Long, Long> o1, Entry<Long, Long> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
      
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on reversed natural ordering
        LinkedHashMap<Long, Long> sortedMap = new LinkedHashMap<Long, Long>();
      
        for(Map.Entry<Long, Long> entry: entries){
            sortedMap.put(entry.getKey(), entry.getValue());
        }      
        return sortedMap.entrySet().iterator().next();
    }
}
